# Chagelog

## CampaRi 0.9.2 | 2019-01-11
#### _Minor changes_
* Error handlings reworked
* Testing and codecoverage reworked with more general code (ev_it)
* Rewriting of find_mahalanobis system in R with consequent optimization and working examples.

## CampaRi 0.9.1 | 2018-04-25
#### _Major changes_
* New version. New barrier recognition. New life.
* Merging new SBR
* Full gitlab migration and correct pipeline execution with docker

Analysis algorithms for time series data. The principal objective of this work is to provide automatic tools for pre-processing and visualization of the raw data, keeping in mind the size of it. The package comprises also a model dedicated section (markov state models). Moreover, we also extracted original algorithms from the main core 'campari' software. For more information please visit the original documentation on http://campari.sourceforge.net/index.html.


## CampaRi 0.8.8 | 2017-06-30
#### _Minor changes_
This release is the first to be fully supported. We are still working on solidifying the Fortran backend and to make it possible the CRAN uploading. Stay tuned. Stay shaken, not stirred.

## CampaRi 0.8.5 | 2017-06-30
#### _Minor changes_
* Roxygenizing