!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 2.0                                                           !
!                                                                          !
!    Copyright (C) 2014, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger,               !
!                        Davide Garolini                                   !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN:      Andreas Vitalis                                               !
! WRAPPER:   Davide Garolini                                               !
!                                                                          !
!--------------------------------------------------------------------------!

! Notes on n_xyz: this variable could be found in the chainsaw program (main)
! There is called this function:
! ! sanity checks and setup for structural clustering
!   if (cstorecalc.le.nsim) then
!     call read_clusteringfile()
!   end if
! In this function the n_xyz variable is set to keep the coordinates bounds
! (e.g. xyz = 1 and 3)
!
!-----------------------------------------------------------------------------
!
subroutine snap_to_cluster_d(val, it, sn2) !i={1:n_snaps}
  use mod_clustering
  use mod_variables_gen
  use mod_distance
  implicit none

  real(8) , INTENT(IN) :: sn2(n_xyz) ! Array of coo of a single snap (i)
  type(t_scluster), INTENT(IN) :: it ! reference cluster
  real(8), INTENT(OUT) :: val ! this is the distance from the clu (tmp_d)
  real(8) vec1(n_xyz) ! n_xyz is the coordinates allocation size
  if(it%nmbrs.eq.0) then
    val = 0
  else
    ! dis_method can be dependent on this
    vec1 = it%sums(1:n_xyz)/(1.0 * it%nmbrs) ! cluster center
    call distance(val,vec1,sn2) ! val=tmp_d, vec1=centroid, sn=snapshot
  end if
end
!
!------------------------------------------------------------------------------------
!
subroutine cluster_to_cluster_d(val, it1, it2)
  use mod_variables_gen
  use mod_clustering
  use mod_distance
  implicit none

  type(t_scluster), INTENT(IN):: it1, it2
  real(8) vec1(n_xyz), vec2(n_xyz)
  real(8), INTENT(OUT) :: val

  ! dis_method can be dependent on this
  vec1 = it1%sums(:)/(1.0 * it1%nmbrs) ! center it1
  vec2 = it2%sums(:)/(1.0 * it2%nmbrs) ! center it2
  call distance(val, vec1, vec2)
end
!
!-------------------------------------------------------------------------------
!
subroutine cluster_addsnap_ix(it,i)
  use mod_clustering
  use mod_variables_gen
  implicit none

  type (t_scluster), INTENT(INOUT) :: it !cluster it is modified by adding i
  integer, intent(in) :: i

  if (it%nmbrs.eq.0) then !case in which it is the first element of the cluster
    if (allocated(it%sums).EQV..false.) then
      allocate(it%sums(n_xyz))
      it%sums(:) = 0.0
    else
      it%sums(:) = 0.0
    end if
    it%sqsum = 0.0
    it%nmbrs = 1
    if (it%nmbrs.gt.it%alsz) then
      it%alsz = 2
      if (allocated(it%snaps).EQV..true.) deallocate(it%snaps)
      allocate(it%snaps(it%alsz))
    end if
  else ! regular add
    it%nmbrs = it%nmbrs + 1 !number of elements in the cluster updating
    if (it%nmbrs.gt.it%alsz) then
      call cluster_resize(it)
    end if
  end if

  it%snaps(it%nmbrs) = i !add the snap

end subroutine cluster_addsnap_ix
!-------------------------------------------------------------------------------
subroutine cluster_addsnap(it,sn1,i)
  use mod_clustering
  use mod_variables_gen
  implicit none

  type (t_scluster), INTENT(INOUT) :: it !cluster it is modified by adding i
  real(8), INTENT(IN) :: sn1(n_xyz) !snap that must be added (vector of positions)
  integer, intent(in) :: i !id of the snap
  ! for dist = 1 dihedral
  integer clstsz, k
  real(8) normer, normer2, incr
  ! real(8) , intent(in) :: r ! distance from the center of the cluster

  if (it%nmbrs.eq.0) then !case in which it is the first element of the cluster
    if (allocated(it%sums).EQV..false.) then
      allocate(it%sums(n_xyz))
      it%sums(:) = 0.0
    else
      it%sums(:) = 0.0
    end if
    it%sqsum = 0.0
    it%nmbrs = 1
    if (it%nmbrs.gt.it%alsz) then
      it%alsz = 2
      if (allocated(it%snaps).EQV..true.) deallocate(it%snaps)
      allocate(it%snaps(it%alsz))
    end if
    it%center = i
  else ! regular add
    it%nmbrs = it%nmbrs + 1 !number of elements in the cluster updating
    if (it%nmbrs.gt.it%alsz) then
      call cluster_resize(it)
    end if
  end if

  it%snaps(it%nmbrs) = i !add the snap
  clstsz = n_xyz
  !cludata = sn1
  if(dis_method .eq. 1) then
    if (it%nmbrs.gt.1) then
      normer2 = 1.0/(1.0*it%nmbrs-1.0)
    else
      normer2 = 0.0
    end if
    normer = 1.0/(1.0*it%nmbrs)
  !   this is approximate: the average mustn't shift very much and/or the points need to be well-clustered
    do k=1,clstsz
      incr = 0.0
      if (sn1(k)-normer2*it%sums(k).lt.-180.0) incr = 360.0
      if (sn1(k)-normer2*it%sums(k).gt.180.0) incr = -360.0
      incr = incr + sn1(k)
      it%sums(k) = it%sums(k) + incr
      it%sqsum = it%sqsum + incr*incr
      if (normer*it%sums(k).lt.-180.0) then
        it%sqsum = it%sqsum + it%nmbrs*360.0*360.0 + 720.0*it%sums(k)
        it%sums(k) = it%sums(k) + it%nmbrs*360.0
      end if
      if (normer*it%sums(k).gt.180.0) then
        it%sqsum = it%sqsum + it%nmbrs*360.0*360.0 - 720.0*it%sums(k)
        it%sums(k) = it%sums(k) - it%nmbrs*360.0
      end if
    end do
  else if(dis_method .eq. 5) then
    it%sums(1:n_xyz) = it%sums(1:n_xyz) + sn1(1:n_xyz) !
    it%sqsum = it%sqsum + dot_product(sn1(1:n_xyz), sn1(1:n_xyz))
  end if
end
!
!-------------------------------------------------------------------------------
!
subroutine cluster_resize(it)
  use mod_clustering
  use mod_variables_gen
  implicit none

  type(t_scluster) it
  integer tmpa(it%alsz), oldsz

  if (allocated(it%snaps).EQV..false.) then
    allocate(it%snaps(2))
    it%alsz = 2
    if (it%alsz.ge.it%nmbrs) return
  end if
  oldsz = it%alsz
  tmpa(:) = it%snaps(:)
  deallocate(it%snaps)
  it%alsz = it%alsz * 2
  if(it%alsz.gt.n_snaps) it%alsz = n_snaps ! dont exagerate with space
  allocate(it%snaps(it%alsz)) ! reallocate snaps using allocation size of *2
  it%snaps(:) = 0 ! dont make fortran invent variables when not initialized
  it%snaps(1:oldsz) = tmpa(:) ! copy the old array into the new
end
!
!-----------------------------------------------------------------------------------------
!
subroutine join_clusters(itl,its)

  use mod_clustering
  use mod_variables_gen
!
  implicit none
!

  type (t_scluster) its, itl

  !for dihedral angles dist = 1
  real(8) normer, normer2, normer3
  integer clstsz
  integer k
  real(8) incr

  clstsz = n_xyz

  if (dis_method .eq. 1) then
    normer2 = 1.0/(1.0*itl%nmbrs)
    normer3 = 1.0/(1.0*its%nmbrs)
    normer = 1.0/(1.0*itl%nmbrs+its%nmbrs)
!   this is approximate: the average mustn't shift very much and/or the points need to be well-clustered
    itl%sqsum = itl%sqsum + its%sqsum
    do k=1,clstsz
      incr = 0.0
      if (normer3*its%sums(k)-normer2*itl%sums(k).lt.-180.0) then
        incr = its%nmbrs*360.0
        itl%sqsum = itl%sqsum + its%nmbrs*360.0*360.0 + 720.0*its%sums(k)
      end if
      if (normer3*its%sums(k)-normer2*itl%sums(k).gt.180.0) then
        incr = -its%nmbrs*360.0
        itl%sqsum = itl%sqsum + its%nmbrs*360.0*360.0 - 720.0*its%sums(k)
      end if
      incr = incr + its%sums(k)
      itl%sums(k) = itl%sums(k) + incr
      if (normer*itl%sums(k).lt.-180.0) then
        itl%sqsum = itl%sqsum + (itl%nmbrs+its%nmbrs)*360.0*360.0 + 720.0*itl%sums(k)
        itl%sums(k) = itl%sums(k) + (itl%nmbrs+its%nmbrs)*360.0
      end if
      if (normer*itl%sums(k).gt.180.0) then
        itl%sqsum = itl%sqsum + (itl%nmbrs+its%nmbrs)*360.0*360.0 - 720.0*itl%sums(k)
        itl%sums(k) = itl%sums(k) - (itl%nmbrs+its%nmbrs)*360.0
      end if
    end do
  else if(dis_method .eq. 5) then
    itl%sums(1:n_xyz) = itl%sums(1:n_xyz) + its%sums(1:n_xyz)
    itl%sqsum = itl%sqsum + its%sqsum
  end if
!
  itl%nmbrs = itl%nmbrs + its%nmbrs
  do while (itl%nmbrs.gt.itl%alsz)
    call cluster_resize(itl)
  end do
  itl%snaps(itl%nmbrs-its%nmbrs+1:itl%nmbrs) = its%snaps(1:its%nmbrs)
  its%nmbrs = 0
  its%alsz = 0
  deallocate(its%snaps)
  deallocate(its%sums)

end
!
!-------------------------------------------------------------------------------
!
subroutine cnbl_resz(i2)
  use mod_gen_nbls
  implicit none

  type(t_cnblst), INTENT(INOUT) :: i2 !it will be the i-element of the cnblst
  ! type(t_cnblst) cnblst
  integer oldalsz
  real(8) distmp(i2%alsz)
  integer idxtmp(i2%alsz)
!
  distmp(:) = i2%dis(:)
  idxtmp(:) = i2%idx(:)
  deallocate(i2%dis)
  deallocate(i2%idx)
  oldalsz = i2%alsz
  i2%alsz = i2%alsz*2
  allocate(i2%dis(i2%alsz))
  allocate(i2%idx(i2%alsz))
  i2%dis(1:oldalsz) = distmp(:)
  i2%idx(1:oldalsz) = idxtmp(:)
!
end
